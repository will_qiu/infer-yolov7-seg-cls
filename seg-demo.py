import  logging, cv2, sys, os, time
from argparse import ArgumentParser, SUPPRESS
from common import read_json, config_logger, Seg, abs_path, AOLLOW_FORMAT, draw

SAVE_KEY = 0

def build_argparser():
    parser = ArgumentParser(add_help=False)
    args = parser.add_argument_group('Options')
    args.add_argument('-h', '--help', action='help', default=SUPPRESS, help='Show this help message and exit.')
    args.add_argument('-c', '--config', required=True, help = "The path of application config")
    return parser

def path_process(config):
    for key, val in config.items():
        if "path" in key and val != "":
            config[key] = abs_path(val)
    return config

def display_image(frame):
    cv2.namedWindow('Detection', 0)
    cv2.imshow("Detection", frame)
    key = cv2.waitKey(0)
    ESC_KEY = 27
    # Quit.
    if  key in {ord('q'), ord('Q'), ESC_KEY}:
        cv2.destroyAllWindows()

def save_images(frame):
    global SAVE_KEY
    cv2.imwrite(f"result_{SAVE_KEY}.jpg", frame)
    SAVE_KEY += 1
    
def process_image(file_path, seg, display=True, save=False):
    image = cv2.imread(file_path)
    if image is not None:
        logging.info(f"Displaying image: {file_path}")
        image = infer_action(seg, image)
        if save:
            save_images(image)
            
        if display:
            display_image(image)
    else:
        logging.error(f"Could not read the image: {file_path}")

def process_video(file_path, seg, display=True, save=False):
    if file_path.isnumeric():
        file_path = int(file_path)
    else:
        file_path = file_path
    cap = cv2.VideoCapture(file_path)
    if not cap.isOpened():
        logging.error(f"Could not open video: {file_path}")
        return

    while True:
        ret, frame = cap.read()
        if not ret:
            break
        frame = infer_action(seg, frame)
        if save:
            save_images(frame)

        if display:
            cv2.namedWindow('Detection', 0)
            cv2.imshow('Detection', frame)
            key = cv2.waitKey(1)
            ESC_KEY = 27
            # Quit.
            if  key in {ord('q'), ord('Q'), ESC_KEY}:
                break
        
    cap.release()
    cv2.destroyAllWindows()   

def process_directory(directory_path, seg, display, save):
    for root, _, files in os.walk(directory_path):
        for file in files:
            file_path = os.path.join(root, file)
            if file_path.lower().endswith(tuple(AOLLOW_FORMAT['image'])):
                process_image(file_path, seg, display, save)

def infer_action(seg, frame):
    # Seg-detection
    seg_start = time.time()
    roi_list, resized_image = seg.detect(frame)
    seg_detec_time = time.time() - seg_start
    logging.info(f"Seg-detection time: { seg_detec_time } s")
    
    if len(roi_list) != 0:
        # Draw
        draw_start = time.time()
        frame = draw(frame, resized_image, roi_list, seg.color_list)
        end = time.time()
        draw_time = end - draw_start
        logging.info(f"Drawing time: {draw_time} s")
    else:
        end = time.time()
        
    final_time = end - seg_start
    logging.info(f"All time: {final_time} s")
    logging.warning("All FPS:{}".format( round(1/final_time, 3) ))
    logging.info("-"*50)
    
    return frame
    
def main(args):
    # Setting config
    config = read_json(args.config)
    # Abs process
    config = path_process(config)
    
    # Loading model
    logging.warning(f"Loading segmentation model...")
    seg = Seg(config["seg_model_path"], conf_thres=config["seg_conf_thres"], iou_thres=config["seg_iou_thres"], classes=config['seg_label_path'])
    
    if os.path.isfile(config['source']):
        if config['source'].lower().endswith(tuple(AOLLOW_FORMAT['image'])):
            process_image(config['source'], seg, config['display'], config['save_result'])
        elif config['source'].lower().endswith(tuple(AOLLOW_FORMAT['video'])):
            process_video(config['source'], seg, config['display'], config['save_result'])
        else:
            logging.error("Unsupported file type.")
    elif os.path.isdir(config['source']):
        process_directory(config['source'], seg, config['display'], config['save_result'])
    else:
        logging.error("Invalid input path.")
        
if __name__ == '__main__':
    config_logger('./seg.log', 'w', "info")
    args = build_argparser().parse_args()
    sys.exit(main(args) or 0)