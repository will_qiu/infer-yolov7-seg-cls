# Inference using yolov7-segmentation & classification of onnx
This is code is inference one class onnx model for yolov7-segmetation and using classification onnx model to classify images.

## Getting Started

## Ubuntu
### Pre-requirements

### Docker
Install **nvidia-driver**, **nvidia-docker** and **docker** before installing the docker container.

- [Tutorial-nvidia-driver](https://docs.nvidia.com/datacenter/tesla/tesla-installation-notes/index.html)

- [Tutorial-docker](https://docs.docker.com/engine/install/ubuntu/)

- [Tutorial-nvidia-docker](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker)

- **Add docker to sudo group** 
    - [Tutourial](https://docs.docker.com/engine/install/linux-postinstall/)
    ``` 
    sudo groupadd docker
    sudo usermod -aG docker $USER
    sudo chmod 777 /var/run/docker.sock
    ```

- Build docker
    ```shell
    sudo chmod u+x ./docker/*.sh
    ./docker/build.sh
    ```
- Run container
    ```shell
    sudo chmod u+x ./docker/*.sh
    xhost +
    ./docker/run.sh
    ```

### Anaconda
- Install **anaconda** for python env
    - [Tutorial-anaconda](https://docs.anaconda.com/free/anaconda/install/linux/)
- Python ENV

    **CPU**
    ```shell
    conda create -n yolov7_seg_cls python=3.10 -c conda-forge -y
    conda activate yolov7_seg_cls
    pip install -r ./docker/requirements.txt
    ```

    **GPU**
    - Required: **nvidia-driver==525** / **nvidia-cuda==11.8.0** / **nvidia-cudnn==8**

    ```shell
    conda create -n yolov7_seg_cls_gpu python=3.10 -c conda-forge -y
    conda activate yolov7_seg_cls_gpu
    pip install -r requirements-gpu.txt
    ```

### Run code
```shell
python3 seg-cls-demo.py -c config/config.json
python3 seg-demo.py -c config/config.json
python3 cls-demo.py -c config/config.json
```
<details>
    <summary> Result
    </summary>
    <div align="center">
        <img width="80%" height="80%" src="./docs/command.png">
    </div>
</details>

### Config
This involves changing the model path and configuring parameters.
- path : config/config.json

```json
{
    "seg_model_path":"model/seg/model.onnx",
    "seg_label_path":"",
    "cls_model_path":"model/cls/model.onnx",
    "cls_label_path":"model/cls/class_names.pickle",
    "source":"data/",
    "display": true,
    "save_result": true,
    "seg_iou_thres":0.45,
    "seg_conf_thres":0.25,
    "cls_conf_thres":0.01
}
```