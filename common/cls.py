import os
import numpy as np
from .model import Model
from .utils import softmax, resize_image, read_pickle, gen_color

class Cls(Model):
    def __init__(self, model_path, conf_thres=0.0, classes=None, select_num=1):
        super().__init__(model_path)
        self.conf_thres = float(conf_thres)
        self.classes = read_pickle(classes) if classes and os.path.exists(classes) else [ str(i) for i in range(1000) ]
        # Generate color
        self.color_list = gen_color(len(self.classes)) if self.classes else gen_color(1000)
        self.select_num = select_num
        
    def preprocess(self, inputs):
        self.image = inputs
        self.image_3c = resize_image(self.image, [self.w, self.h] )
        
        self.image_4c = self.image_3c / 255.0
        ### AIDMS NO Normalization
        # mean = np.array([0.485, 0.456, 0.406])
        # std = np.array([0.229, 0.224, 0.225])
        # self.image_4c = (self.image_4c - mean) / std
        self.image_4c = np.transpose(self.image_4c, (2, 0, 1)).astype(np.float32)
        self.image_4c = np.expand_dims(self.image_4c, axis=0)

    def detect(self, frame):
        self.preprocess(frame)
        self.infer(self.image_4c)
        self.postprocess()
        return self.detections

    def postprocess(self):
        self.detections = {}
        result = self.outputs[0][0]
        pred = softmax(result)
        # Find the class with the highest probability
        top_classes = np.argsort(pred)[::-1] 
        top_confidences = pred[top_classes] 
        # Filter results based on the threshold
        selected_indices = top_classes[top_confidences >= self.conf_thres]
        selected_confidences = top_confidences[top_confidences >= self.conf_thres]
        # Get the top five classes and confidences
        self.detections["indices"] = list(selected_indices[:self.select_num])
        self.detections["confidences"] =  list(selected_confidences[:self.select_num])
        self.detections["labels"] = [ str(i) for i in self.detections["indices"] ]
        if self.classes:
            self.detections["labels"]  = [self.classes[i] for i in self.detections["indices"] ]
