import cv2, os, logging, json, pickle
import numpy as np

AOLLOW_FORMAT = {
                    "image": [".jpg", ".bmp", ".png", ".jpeg", ".JPG", ".JPEG", ".PNG", ".BMP"],
                    "annotation": [".txt", ".xml", ".json"],
                    "video": [".mp4", ".avi", ".mov", ".mkv", ".MP4", ".AVI", ".MOV", ".MKV"]
                }

def sigmoid(x): 
    return 1.0/(1+np.exp(-x))

def softmax(x):
    x -= np.max(x)
    exp_x = np.exp(x)
    return exp_x / exp_x.sum(axis=0)

def read_txt(path:str):
    with open(path) as f:
        return f.read()

def read_pickle(path:str):
    with open(path, 'rb') as file:
        pickled_data = pickle.load(file)
    return pickled_data

def read_json(path:str):
    if os.path.exists(path):
        with open(path) as f:
            return json.load(f)
    else:
        logging.error("This path of .json does not exist:{}".format(path))
 
def abs_path(path:str):
    return path if os.path.isabs(path) else os.path.abspath(path)
                      
def gen_color(class_num):
    color_list = []
    np.random.seed(1)
    while 1:
        a = list(map(int, np.random.choice(range(255),3)))
        if(np.sum(a)==0): continue
        color_list.append(a)
        if len(color_list)==class_num: break
    return color_list

def get_classes_list(classes_path:str):
    return [cls for cls in read_txt(classes_path).split("\n") if cls !=""]

def resize_image(image, size, keep_aspect_ratio=False, interpolation=cv2.INTER_LINEAR):
    if not keep_aspect_ratio:
        resized_frame = cv2.resize(image, size, interpolation=interpolation)
    else:
        h, w = image.shape[:2]
        scale = min(size[1] / h, size[0] / w)
        resized_frame = cv2.resize(image, None, fx=scale, fy=scale, interpolation=interpolation)
    return resized_frame

def put_highlighted_text(frame, message, position, font_face, font_scale, color, thickness):
    cv2.putText(frame, message, position, font_face, font_scale, (255, 255, 255), thickness + 5) # white border
    cv2.putText(frame, message, position, font_face, font_scale, color, thickness) 
    
def draw(frame, resized_image, roi_list, color_list):
    orig_shape = frame.shape[:-1]
    mask_img = np.zeros_like(resized_image)
    for roi in roi_list:
        cv2.rectangle(resized_image, (int(roi['box'][0]), int(roi['box'][1])), (int(roi['box'][2]), int(roi['box'][3])), color_list[int(roi["indices"])], 3, 4)
        logging.warning(f"Detecting - Label: {roi['labels']} | Confidence: {round( float(roi['confidences']), 3)}")
        cv2.putText(resized_image, f"{roi['labels']}", (int(roi['box'][0]), int(roi['box'][1]-10)), cv2.FONT_HERSHEY_SIMPLEX, 1, color_list[int(roi["indices"])], 2)
        mask_img[roi["mask"]!=0] = color_list[int(roi["indices"])]
        resized_image = cv2.addWeighted(resized_image, 0.5, mask_img, 0.5, 0)
    return resize_image(resized_image, orig_shape[::-1])

def draw_cls(frame, roi_list, font_scale = 0.7):
    classes_len = len(roi_list['indices'])
    class_label = ""
    label_height = cv2.getTextSize(class_label, cv2.FONT_HERSHEY_COMPLEX, font_scale, 2)[0][1]
    initial_labels_pos =  frame.shape[0] - label_height * (int(1.5 * classes_len) + 1)

    if (initial_labels_pos < 0):
        initial_labels_pos = label_height
        logging.warning('Too much labels to display on this frame, some will be omitted')
    offset_y = initial_labels_pos

    header = "Label:     Score:"
    label_width = cv2.getTextSize(header, cv2.FONT_HERSHEY_COMPLEX, font_scale, 2)[0][0]
    put_highlighted_text(frame, header, (frame.shape[1] - label_width, offset_y),
                                    cv2.FONT_HERSHEY_COMPLEX, font_scale, (255, 0, 0), 2)
    for index in range(classes_len):
        idx = roi_list['indices'][index]
        class_label = roi_list['labels'][index]
        score = roi_list['confidences'][index]
        label = '{}. {}    {:.2f}'.format(idx, class_label, score)
        label_width = cv2.getTextSize(label, cv2.FONT_HERSHEY_COMPLEX, font_scale, 2)[0][0]
        offset_y += int(label_height * 1.5)
        put_highlighted_text(frame, label, (frame.shape[1] - label_width, offset_y),
            cv2.FONT_HERSHEY_COMPLEX, font_scale, (255, 0, 0), 2)
        logging.warning(f"Detecting - Label: {class_label} | Confidence: {round( float(score), 3)}")
        
    return frame