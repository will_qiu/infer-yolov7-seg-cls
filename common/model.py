import logging
import numpy as np
import onnxruntime

class Model:
    def __init__(self, model_path):
        self.sess = onnxruntime.InferenceSession(model_path, providers=['CUDAExecutionProvider'])
        # Single input node info
        input_info = self.sess.get_inputs()
        self.input_node = input_info[0].name
        logging.info(f"Model input name:[{self.input_node}]")
        self.w = input_info[0].shape[2]
        self.h = input_info[0].shape[3]
        logging.info(f"Model input shape:[{self.w}, {self.h}]")
        # Multi output node info
        output_info = self.sess.get_outputs()
        self.outputs_node = [ output_tensor.name for output_tensor in output_info]
        logging.info(f"Model outputs name:{self.outputs_node}")
        # Warm up the model with multiple runs
        input_data = np.random.rand(1, 3, self.w, self.h).astype(np.float32)
        logging.warning("Warming up...!")
        warmup_runs = 10
        for _ in range(warmup_runs):
            self.sess.run(None, {self.input_node: input_data})

    def preprocess(self, inputs):
        raise NotImplementedError
    
    def infer(self, inputs):
        self.outputs = self.sess.run( self.outputs_node,  {self.input_node: inputs} )
    
    def postprocess(self, outputs):
        raise NotImplementedError